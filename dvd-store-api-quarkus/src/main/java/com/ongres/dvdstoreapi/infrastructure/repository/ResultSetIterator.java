package com.ongres.dvdstoreapi.infrastructure.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.function.Function;

import com.ongres.dvdstoreapi.domain.ports.repository.RepositoryException;

/**
 * The <b>ResultSetIterator</b> is a class that implements the Iterator, Iterable and AutoClosable interfaces.
 * 
 * The Iterator interface lets iterate the elements of the ResultSet in a forward direction only.
 * The Iterable interface lets the class be target of the so calledd "for-each loop" statemenr.
 * The AutoClosable interface lets the class be target of try-with-resources statement in which each resource is closed
 * automatically upon completion of the statement.
 * 
 */
final class ResultSetIterator<T> implements Iterator<T>, Iterable<T>, AutoCloseable {

  // The connection used to retrieve the ResultSet
  final Connection connection;
  // The statement used to perform the SQL query
  final PreparedStatement statement;
  // The ResultSet used to retrieve the result
  final ResultSet resultSet;
  // A Function that is applied for each row of the ResultSet to an ojbect of type T
  final Function<ResultSet, T> mapper;

  ResultSetIterator(
      Connection connection,
      PreparedStatement statement,
      ResultSet resultSet,
      Function<ResultSet, T> mapper) {
    this.connection = connection;
    this.statement = statement;
    this.resultSet = resultSet;
    this.mapper = mapper;
  }


  /**
   * 
   * @return true if Connection, PreparedStatement and ResultSet are closed
   *         and is not possible to execute a SQL query and retrieve the result,
   *         false otherwise
   * 
   * @throws RepositoryException is some error happens
   */
  public boolean isClosed() {
    try {
      return !connection.isClosed()
            && !statement.isClosed()
            && !resultSet.isClosed();
    } catch (final SQLException ex) {
      throw new RepositoryException(ex);
    }
  }

  /**
   * @return true if there is one more element in the ResultSet, false otherwise
   * @throws RepositoryException if some error happens
   */
  @Override
  public boolean hasNext() {
    try {
      boolean next = !isClosed() 
          && resultSet.next();
      if (!next)
        close();
        
      return next;
    } catch (SQLException ex) {
      throw new RepositoryException(ex);
    }
  }

  /**
   * @return T if there is one more element in the cursor to iterate over and apply a mapper function to it
   */
  @Override
  public T next() {
    return mapper.apply(resultSet);
  }

  /**
   * @return Iterator<T> iterator that can be target of for-each statement
   */
  @Override
  public Iterator<T> iterator() {
    return new ResultSetIterator<>(connection, statement, resultSet, mapper);
  }

  /**
   * @return if not closed, close Connection, PreparedStatement and ResultSet
   * @throws RepositoryException if some error happens
   */
  @Override
  public void close() {
    if (!isClosed()) {
      try {
        connection.close();
        statement.close();
        resultSet.close();
      } catch (final SQLException ex) {
        throw new RepositoryException(ex);
      }
    }
  }
}

package com.ongres.dvdstoreapi.infrastructure.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.ongres.dvdstoreapi.domain.model.ActorFilm;
import com.ongres.dvdstoreapi.domain.model.Count;
import com.ongres.dvdstoreapi.domain.model.ImmutableCount;
import com.ongres.dvdstoreapi.domain.model.RentalOverdue;
import com.ongres.dvdstoreapi.domain.ports.repository.CustomersRepository;
import com.ongres.dvdstoreapi.domain.ports.repository.FilmsRepository;
import com.ongres.dvdstoreapi.domain.ports.repository.NotFoundDataException;
import com.ongres.dvdstoreapi.domain.ports.repository.RentalRepository;
import com.ongres.dvdstoreapi.domain.ports.repository.RepositoryException;
import io.agroal.api.AgroalDataSource;
import org.jooq.lambda.Unchecked;

@ApplicationScoped
public class PostgresRepository
  implements CustomersRepository, FilmsRepository, RentalRepository {

  @Inject
  AgroalDataSource dataSource;

  @Override
  public Count countByCountry(String country) {
    try (
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(
          """
          SELECT COUNT(*)
          FROM customer
          JOIN address USING (address_id)
          JOIN city USING (city_id)
          JOIN country USING (country_id)
          WHERE country = ?
          GROUP BY country
          HAVING country IS NOT NULL;
          """)) {
      statement.setString(1, country);
      try (ResultSet resulSet = statement.executeQuery()) {
        if (!resulSet.next()) {
          throw new NotFoundDataException("Country " + country + " not found");
        }
        return ImmutableCount.builder()
            .count(resulSet.getInt(1))
            .build();
      }
    } catch (SQLException ex) {
      throw new RepositoryException(ex);
    }
  }

  @Override
  public Count countByCountryAndCity(String country, String city) {
    try (
        Connection connection = dataSource.getConnection();
        PreparedStatement statement = connection.prepareStatement(
          """
          SELECT COUNT(*)
          FROM customer
          JOIN address USING (address_id)
          JOIN city USING (city_id)
          JOIN country USING (country_id)
          WHERE country = ?
          AND city = ?
          GROUP BY city
          HAVING city IS NOT NULL;
          """)) {
      statement.setString(1, country);
      statement.setString(2, city);
      try (ResultSet resulSet = statement.executeQuery()) {
        if (!resulSet.next()) {
          throw new NotFoundDataException("Country " + country + " or city " + city + " not found");
        }
        return ImmutableCount.builder()
            .count(resulSet.getInt(1))
            .build();
      }
    } catch (SQLException ex) {
      throw new RepositoryException(ex);
    }
  }

  @Override
  public Stream<ActorFilm> filmByActor(String actor) {
    return Stream.of(this)
        .flatMap(o -> filmByActorSupplier(actor));
  }

  Stream<ActorFilm> filmByActorSupplier(String actor) {
    Closer closer = new Closer();
    try {
      Connection connection = closer.append(dataSource.getConnection());
      PreparedStatement statement = closer.append(connection.prepareStatement(
          """
          WITH found_actor AS (
            SELECT actor_id, first_name, last_name
            FROM actor
            WHERE UPPER(first_name) = UPPER(?)
            OR UPPER(last_name) = UPPER(?)
            OR UPPER(first_name || ' ' || last_name) = UPPER(?)
            OR UPPER(last_name || ' ' || first_name) = UPPER(?)
          ),
          count_found_actor AS (
            SELECT COUNT(*) AS count FROM found_actor
          )
          SELECT null, null, null, null, null
          FROM count_found_actor
          WHERE count > 0
          UNION ALL
          SELECT
            found_actor.first_name,
            found_actor.last_name,
            film.title,
            film.description,
            STRING_AGG(DISTINCT category.name, ', ') AS category
          FROM found_actor
          JOIN film_actor USING (actor_id)
          JOIN film USING (film_id)
          JOIN film_category USING (film_id)
          JOIN category USING (category_id)
          GROUP BY (found_actor.first_name, found_actor.last_name, film.title, film.description)
          """));
      statement.setString(1, actor);
      statement.setString(2, actor);
      statement.setString(3, actor);
      statement.setString(4, actor);
      ResultSet resulSet = closer.append(statement.executeQuery());
      if (!resulSet.next()) {
        throw new NotFoundDataException("Actor " + actor + " not found");
      }
      return StreamSupport.stream(
          Spliterators.spliteratorUnknownSize(
              new ResultSetIterator<>(
                  connection, statement, resulSet,
                  new ActorFilmResultSetMapper(1, 2, 3, 4, 5)::toActorFilm),
              Spliterator.ORDERED),
          false)
          .onClose(Unchecked.runnable(closer::close));
    } catch (SQLException ex) {
      throw closer.close(new RepositoryException(ex));
    }
  }

  @Override
  public Stream<RentalOverdue> rentalOverdues() {
    return Stream.of(this)
        .flatMap(o -> rentalOverduesSupplier());
  }

  Stream<RentalOverdue> rentalOverduesSupplier() {
    Closer closer = new Closer();
    try {
      Connection connection = closer.append(dataSource.getConnection());
      PreparedStatement statement = closer.append(connection.prepareStatement(
          """
          SELECT
            customer.last_name || ', ' || customer.first_name,
            address.phone,
            film.title
          FROM customer
          JOIN address USING (address_id)
          JOIN rental USING (customer_id)
          JOIN inventory USING (inventory_id)
          JOIN film USING (film_id)
          WHERE return_date IS NULL AND CURRENT_DATE > rental_date + (rental_duration || ' days')::interval 
          """));
      ResultSet resulSet = closer.append(statement.executeQuery());
      return StreamSupport.stream(
          Spliterators.spliteratorUnknownSize(
              new ResultSetIterator<>(
                  connection, statement, resulSet,
                  new RentalOverdueResultSetMapper(1, 2, 3)::toRentalOverdue),
              Spliterator.ORDERED),
          false)
          .onClose(Unchecked.runnable(closer::close));
    } catch (SQLException ex) {
      throw closer.close(new RepositoryException(ex));
    }
  }

}

package com.ongres.dvdstoreapi.domain.ports.repository;

import javax.annotation.Nonnull;

import com.ongres.dvdstoreapi.domain.model.Count;

public interface CustomersRepository {

  @Nonnull Count countByCountry(@Nonnull String country);

  @Nonnull Count countByCountryAndCity(
      @Nonnull String country,
      @Nonnull String city);

}

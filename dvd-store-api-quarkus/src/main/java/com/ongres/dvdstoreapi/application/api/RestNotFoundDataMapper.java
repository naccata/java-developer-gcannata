package com.ongres.dvdstoreapi.application.api;

import javax.enterprise.context.ApplicationScoped;

import com.ongres.dvdstoreapi.domain.ports.repository.NotFoundDataException;
import io.quarkus.vertx.web.Route;
import io.quarkus.vertx.web.Route.HandlerType;
import io.vertx.core.http.HttpServerResponse;

@ApplicationScoped
public class RestNotFoundDataMapper {

  @Route(type = HandlerType.FAILURE, order = 1000)
  public void notFoundData(NotFoundDataException ex, HttpServerResponse response) {
    response.setStatusCode(404).end();
  }

}
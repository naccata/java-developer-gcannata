package com.ongres.dvdstoreapi.application.api;

import javax.inject.Inject;

import com.ongres.dvdstoreapi.domain.model.ActorFilm;
import com.ongres.dvdstoreapi.domain.ports.repository.FilmsRepository;
import com.ongres.dvdstoreapi.domain.ports.service.FilmsService;
import io.quarkus.vertx.web.Param;
import io.quarkus.vertx.web.ReactiveRoutes;
import io.quarkus.vertx.web.Route;
import io.quarkus.vertx.web.Route.HttpMethod;
import io.quarkus.vertx.web.RouteBase;
import io.smallrye.mutiny.Multi;

@RouteBase(path = "/films")
public class RestFilmsService {

  private final FilmsService filmsService;

  @Inject
  public RestFilmsService(
      FilmsRepository filmsRepository) {
    this.filmsService = new FilmsService(filmsRepository);
  }

  @Route(methods = HttpMethod.GET, path = "/find-by-actor/:actor",
    produces = {
        ReactiveRoutes.APPLICATION_JSON,
        ReactiveRoutes.ND_JSON
    })
  public Multi<ActorFilm> filmsByActor(
      @Param("actor") String actor,
      @Param("category") String category
      ) {
    return Multi.createFrom()
        .items(filmsService.filmByActorAndCategory(actor, category));
  }

}
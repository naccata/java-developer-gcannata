package com.ongres.dvdstoreapi.application.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestCustomersServiceIT extends AbstractRestServiceIT {

  @Autowired
  private WebTestClient webTestClient;

  @Test
  public void testCustomersCountWithoutCountry() throws Exception {
    webTestClient.get()
        .uri("/customers/count-by-country")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isNotFound();
  }

  @Test
  public void testCustomersCountForSpain() throws Exception {
    webTestClient.get()
        .uri("/customers/count-by-country/Spain")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody(String.class)
        .isEqualTo("5");
  }

  @Test
  public void testCustomersCountForSalamanca() throws Exception {
    webTestClient.get()
        .uri("/customers/count-by-country/Spain?city=Santiago de Compostela")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody(String.class)
        .isEqualTo("1");
  }

  @Test
  public void testCustomersCountForNotFound() throws Exception {
    webTestClient.get()
        .uri("/customers/count-by-country/NotFound")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isNotFound();
  }

}
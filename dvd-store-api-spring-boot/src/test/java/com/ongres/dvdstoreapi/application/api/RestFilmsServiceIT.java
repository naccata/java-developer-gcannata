package com.ongres.dvdstoreapi.application.api;

import static org.hamcrest.Matchers.hasSize;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RestFilmsServiceIT extends AbstractRestServiceIT {

  @Autowired
  private WebTestClient webTestClient;

  @Test
  public void testFilmsByActorWithoutActor() throws Exception {
    webTestClient.get()
        .uri("/films/find-by-actor")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isNotFound();
  }

  @Test
  public void testFilmsByActorForDanTorn() throws Exception {
    webTestClient.get()
        .uri("/films/find-by-actor/Dan Torn")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$", hasSize(22));
  }

  @Test
  public void testFilmsByActorForDanTornAndClassicsCategory() throws Exception {
    webTestClient.get()
        .uri("/films/find-by-actor/Dan Torn?category=Classics")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isOk()
        .expectBody()
        .jsonPath("$", hasSize(3));
  }

  @Test
  public void testFilmsByActorForActorNotFound() throws Exception {
    webTestClient.get()
        .uri("/films/find-by-actor/Not Found")
        .accept(MediaType.APPLICATION_JSON)
        .exchange()
        .expectStatus().isNotFound();
  }

}
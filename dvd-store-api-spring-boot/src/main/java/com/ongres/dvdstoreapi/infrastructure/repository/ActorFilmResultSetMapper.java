package com.ongres.dvdstoreapi.infrastructure.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.ongres.dvdstoreapi.domain.model.ActorFilm;
import com.ongres.dvdstoreapi.domain.ports.repository.RepositoryException;

class ActorFilmResultSetMapper {
  final Integer[] columnIndexes;
  
  public ActorFilmResultSetMapper(Integer...columnIndexes) {
    this.columnIndexes = columnIndexes;
  }

  ActorFilm toActorFilm(ResultSet resultSet) {
    try {
      return ActorFilm.builder()
          .actorFirstName(resultSet.getString(columnIndexes[0]))
          .actorLastName(resultSet.getString(columnIndexes[1]))
          .filmTitle(resultSet.getString(columnIndexes[2]))
          .filmDescription(resultSet.getString(columnIndexes[3]))
          .filmCategory(resultSet.getString(columnIndexes[4]))
          .build();
    } catch (SQLException ex) {
      throw new RepositoryException(ex);
    }
  }
}

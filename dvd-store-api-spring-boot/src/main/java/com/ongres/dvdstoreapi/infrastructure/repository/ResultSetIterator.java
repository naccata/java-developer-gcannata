package com.ongres.dvdstoreapi.infrastructure.repository;

import java.lang.ref.Cleaner;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.function.Function;

import com.ongres.dvdstoreapi.domain.ports.repository.RepositoryException;

/**
 * A <b>ResultSetIterator<b> implementing the AutoCloseable interface makes it target of try-with-resources, that is,
 * is automatically closed when it exits the aformentioned try-with-resources block.
 * 
 * If not target of a try-with-resources block, a <b>ResultSetIterator</b> resources are automatically released when
 * when the <b>ResultSetIterator</b> becomes phantom reachable thanks to a Cleaner that is registered upon state creation.
 * 
 */
class ResultSetIterator<T> implements AutoCloseable {

  // The Cleanere that holds the Cleanable action
  private static final Cleaner cleaner = Cleaner.create();
  
  // The State that holds the required parameters to perform SQL queries and retrieve results
  static class State<T> implements Runnable, Iterator<T> {

    // The TCP connection over the SQL query is performed
    final Connection connection;
    // The PreparedStatement is the SQL query to be performed
    final PreparedStatement statement;
    // The ResultSet is the result of the SQL query to iterate over
    final ResultSet resultSet;
    // The mapper is a Function that maps the elements of the ResultSet while are iterared over
    final Function<ResultSet, T> mapper;

    State(
      final Connection connection,
      final PreparedStatement statement,
      final ResultSet resultSet,
      final Function<ResultSet, T> mapper
    ) {
      this.connection = connection;
      this.statement = statement;
      this.resultSet = resultSet;
      this.mapper = mapper;
    }

    // The Runnable action that performs the Cleanable cleaning action
    @Override
    public void run() {
      try {
        connection.close();
        statement.close();
        resultSet.close();
      } catch (final SQLException e) {
        throw new RepositoryException(e);
      }
    }

    // The method that tells if there are more items from the ResultSet to iterate over
    @Override
    public boolean hasNext() {
      try {
        boolean next = !connection.isClosed()
            && !statement.isClosed()
            && !resultSet.isClosed() 
            && resultSet.next();
        if (!next) {
          if (!resultSet.isClosed()) {
            resultSet.close();
          }
          if (!statement.isClosed()) {
            statement.close();
          }
          if (!connection.isClosed()) {
            connection.close();
          }
        }
        return next;
      } catch (SQLException ex) {
        throw new RepositoryException(ex);
      }
    }

    // Apply the mapper function and returns the next item in the iterator
    @Override
    public T next() {
      return mapper.apply(resultSet);
    }
  }

  // The State that must be cleaned
  private final State<T> state;
  // The Cleanable that performs the cleaner action
  private final Cleaner.Cleanable cleanable;

  ResultSetIterator(
    final Connection connection,
    final PreparedStatement statement,
    final ResultSet resultSet,
    final Function<ResultSet, T> mapper
  ) {
    this.state = new State<>(connection, statement, resultSet, mapper);
    this.cleanable = cleaner.register(this, state);
  }

  // Performs the cleanable action and close any resources if the State object becomes phantom reachable
  @Override
  public void close() throws Exception {
    cleanable.clean();
  }
  
}

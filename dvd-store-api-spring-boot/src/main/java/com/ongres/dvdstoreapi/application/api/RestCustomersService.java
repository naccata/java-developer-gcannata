package com.ongres.dvdstoreapi.application.api;

import com.ongres.dvdstoreapi.domain.ports.repository.CustomersRepository;
import com.ongres.dvdstoreapi.domain.ports.service.CustomersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")
public class RestCustomersService {

  private final CustomersService customersService;

  @Autowired
  public RestCustomersService(CustomersRepository customersRepository) {
    this.customersService = new CustomersService(customersRepository);
  }

  @GetMapping(value = "/count-by-country/{country}")
  public @ResponseBody int count(
      @PathVariable("country") String country,
      @RequestParam(value = "city", required = false) String city
      ) throws Exception {
    return customersService.countByCountryOrCountryAndCity(country, city)
        .getCount();
  }

}